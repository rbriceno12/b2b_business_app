const express = require('express')
const cors = require('cors')
const app = express()

const auth = require('./auth/index')
const partners = require('./partners/index')
const users = require('./users/index')
const purchaseOrders = require('./purchase-orders/index')
const invoices = require('./invoices/index')
const products = require('./products/index')
const categories = require('./categories/index')
const dashboard = require('./dashboard/index')
const sidebar = require('./sidebar/index')

app.use(cors())
app.use(express.json());
const multer = require('multer');
app.use('/public/uploads', express.static('public/uploads'));

// Configure Multer for file uploads
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'public/uploads/'); // Replace with your desired upload directory
    },
    filename: (req, file, cb) => {
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
      cb(null, file.fieldname + '-' + uniqueSuffix + '.' + file.originalname.split('.').pop());
    }
});
  
const upload = multer({ storage: storage });

app.get('/', function (req, res, next) {console.log('Hello world')})

//Auth
app.post('/auth/register-users/', (req, res) => {
    auth.registerUser(req, res)
})

app.post('/auth/login-users/', (req, res) => {
    auth.loginUser(req, res)
})

app.get('/auth/verify/', auth.verifyToken, (req, res, next) => {
    auth.verifyLoggedUser(req, res)
})

app.post('/auth/forgot-password/', (req, res) => {
    auth.sendEmailForgotPassword(req, res)
})

app.post('/auth/change-password/', (req, res) => {
    auth.changeUserPassword(req, res)
})

//Dashboard
app.get('/dashboard/data/', auth.verifyToken, (req, res) => {
    dashboard.getDashboardData(req, res)
})

//Partners
app.post('/partners/upload-partner/', upload.single('file'), (req, res) => {
    partners.createPartner(req, res)
})

app.get('/partners/cms/', auth.verifyToken, (req, res) => {
    partners.getPartnersCms(req, res)
})

app.get('/partners/public/', (req, res) => {
    partners.getPartnersPublic(req, res)
})

app.post('/partners/:id/:method', auth.verifyToken, (req, res) => partners.updatePartnerStatus(req, res) )


//Users
app.post('/users/create-users/', (req, res) => {
    auth.registerUserNoAuth(req, res)
})

app.get('/users/list/', auth.verifyToken, (req, res) => {
    users.getUsers(req, res)
})

app.get('/users/:id/', auth.verifyToken, (req, res) => {
    users.getUserById(req, res)
})

app.post('/users/:id/:method', auth.verifyToken, (req, res) => users.updateUserStatus(req, res) )


//Purchase orders
app.get('/purchase-orders/list', auth.verifyToken, (req, res) => {
    purchaseOrders.getPurchaseOrders(req, res)
})

app.post('/purchase-orders/list/client/', auth.verifyToken, (req, res) => {
    purchaseOrders.getPurchaseOrdersByClient(req, res)
})

app.post('/purchase-orders/', upload.single('file'), (req, res) => {
    purchaseOrders.createPurchaseOrders(req, res)
})

app.post('/purchase-orders/clients/', upload.single('file'), (req, res) => {
    purchaseOrders.createPurchaseOrdersClients(req, res)
})

app.post('/purchase-orders/:id/:method', auth.verifyToken, (req, res) => purchaseOrders.updatePurchaseOrderStatus(req, res) )

//Invoices
app.get('/invoices/list', auth.verifyToken, (req, res) => {
    invoices.getInvoices(req, res)
})

app.post('/invoices/confirm', auth.verifyToken, (req, res) => {
    invoices.updateInvoiceStatus(req, res)
})

//Clients
app.post('/clients/search/', auth.verifyToken, (req, res) => {
    users.getClients(req, res)
})

//Categories
app.get('/categories/list/', (req, res) => {
    categories.getCategories(req, res)
})

app.get('/categories/list/cms', (req, res) => {
    categories.getCategoriesCms(req, res)
})

app.patch('/categories/:method', (req, res) => {
    categories.updateCategoryStatus(req, res)
})

app.post('/upload-category', upload.single('file'), (req, res) => {
    categories.createCategories(req, res)
});

//Products
app.get('/products/cms/', auth.verifyToken, (req, res) => {
    products.getProducts(req, res)
})

app.patch('/products/update-status/:method/:id', auth.verifyToken, (req, res) => {
    products.updateProductStatus(req, res)
})

app.get('/products/public/', (req, res) => {
    products.getProductsPublic(req, res)
})

//Sidebar
app.get('/sidebar/list/', auth.verifyToken, (req, res) => {
    const userId = req.userId;
    sidebar.listSidebar(userId, req, res);
});

// Route handler to handle product upload
app.post('/upload-product', upload.single('file'), (req, res) => {
    products.createProduct(req, res)
});

app.listen(8000, function () {
  console.log('CORS-enabled web server listening on port 3000')
})