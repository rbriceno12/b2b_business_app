const db = require('../db');

const getUsers = async (req, res) => {
    const itemsPerPage = 10;
    const page = req.query.page || 1

    try {

        const users = await db.query(
            `SELECT u.id, u.name, u.email, u.status, u.created_at, r.name as role
            FROM users u
            INNER JOIN roles r
            ON u.role_id = r.id
            ORDER BY id DESC
            LIMIT ${itemsPerPage}
            OFFSET (${page} - 1) * ${itemsPerPage}`
        )

        return res.status(200).json({
            message: "Users retreived successfully",
            data: users.rows
        })

    } catch (error) {
        return res.status(500).json( {message: "Internal server error"} )
    }
}

const getUserById = async (req, res) => {

    const { id } = req.params

    try {

        const user = await db.query(
            `SELECT u.id, u.name, u.email, u.status, u.created_at, r.name as role
            FROM users u
            INNER JOIN roles r 
            ON u.role_id = r.id
            WHERE u.id = $1`, [id]
        )

        return res.status(200).json({
            message: "Users retreived successfully",
            data: user.rows
        })

    } catch (error) {return res.status(500).json( {message: "Internal server error"} )
    }
}

const updateUserStatus = async (req, res) => {

    const { id, method } = req.params

    let methodString = ''

    switch (method) {
        case 'active':
            methodString = 'active'
            break;
        case 'delete':
            methodString = 'deleted'
            break;
        case 'desactivate':
            methodString = 'desactivated'
            break;
        default:
            break;
    }


    try {

        await db.query(`UPDATE users SET status = '${methodString}' WHERE id = $1`, [id])

        return res.status(200).json({message: "Partner status updated successfully"})

    } catch (error) {

        return res.status(500).json( {message: "Internal server error"} )

    }

}

const getClients = async (req, res) => {

    const itemsPerPage = 5;
    const page = req.query.page || 1
    const q = req.body.q

    console.log('pasamos por aqui')

    try {

        const users = await db.query(
            `SELECT id, name, email
            FROM users
            WHERE status != 'deleted'
            AND role_id = 4
            AND name ilike '%${q}%'
            OR email ilike '%${q}%'
            ORDER BY name ASC
            LIMIT ${itemsPerPage}
            OFFSET (${page} - 1) * ${itemsPerPage}`
        )

        return res.status(200).json({
            message: "Users retreived successfully",
            data: users.rows
        })

    } catch (error) {return res.status(500).json( {error: error, message: "Internal server error"} )
    }
}

module.exports = {
    getUsers: getUsers,
    updateUserStatus: updateUserStatus,
    getClients: getClients,
    getUserById: getUserById
}