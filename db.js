const { Pool } = require('pg');
require('dotenv').config();


// AWS server
const pool = new Pool({
  user: process.env.PG_USER,
  password: process.env.PG_PASSWORD,
  host: process.env.PG_HOST,
  port: process.env.PG_PORT, // default Postgres port
  database: process.env.PG_DATABASE
});


//Localhost
// const pool = new Pool({
//   user: 'postgres',
//   password: 'postgres',
//   host: 'localhost',
//   port: 5432, // default Postgres port
//   database: 'legal_app'
// });

module.exports = {
  query: (text, params) => pool.query(text, params)
};