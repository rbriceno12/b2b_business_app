const db = require('../db');
const helper = require('../helper');
const mailer = require('../mailer');


const getPurchaseOrders = async (req, res) => {
    const itemsPerPage = 20;
    const page = req.query.page || 1

    try {

        const purchaseOrders = await db.query(
            `SELECT p.id, file, u.email, u.id as user_id, p.status, p.created_at, iu.comission
            FROM purchase_orders p
            INNER JOIN users u
            ON p.user_client_id = u.id
            LEFT JOIN invoices_users iu
            ON p.id = iu.purchase_order_id
            WHERE p.status in ('purchase-order', 'cancelled-purchase-order', 'active')
            ORDER BY p.id DESC
            LIMIT ${itemsPerPage}
            OFFSET (${page} - 1) * ${itemsPerPage}`
        )

        return res.status(200).json({
            message: "Purchase orders retreived successfully",
            data: purchaseOrders.rows
        })

    } catch (error) {return res.status(500).json( {message: "Internal server error"} )
    }
}

const getPurchaseOrdersByClient = async (req, res) => {
    const itemsPerPage = 30;
    const page = req.query.page || 1
    const {email} = req.body

    try {

        const purchaseOrders = await db.query(
            `SELECT p.id, file, u.email, p.status, p.created_at, p.value, iu.comission
            FROM purchase_orders p
            INNER JOIN users u
            ON p.user_client_id = u.id
            LEFT JOIN invoices_users iu
            ON p.id = iu.purchase_order_id
            WHERE p.status != 'deleted'
            AND u.email = $1
            ORDER BY p.id DESC
            LIMIT ${itemsPerPage}
            OFFSET (${page} - 1) * ${itemsPerPage}`, [email]
        )

        return res.status(200).json({
            message: "Purchase orders retreived successfully",
            data: purchaseOrders.rows
        })

    } catch (error) {return res.status(500).json( {message: "Internal server error"} )
    }
}

const createPurchaseOrders = async (req, res) => {

    const {user_client_id, name, email} = req.body
    const filename = req.file.filename;


    let idClient = user_client_id

    if(!user_client_id)
        idClient = await createPurchaseOrderClient(name, email)

    try {

        await db.query(`INSERT INTO purchase_orders 
                        (user_client_id, file, status) 
                        VALUES ($1, $2, $3)`, [idClient, filename, 'purchase-order']);
        

        return res.status(200).json({ 
            message: "Purchase order created successfully",
        })

    } catch (error) {
        return res.status(500).json( {message: "Internal server error"} )
    }
}

const createPurchaseOrdersClients = async (req, res) => {

    const {email} = req.body
    const filename = req.file.filename;


    const idClient = await helper.getPurchaseOrderClientByEmail(email)

    try {

        const purchaseOrder = await db.query(`INSERT INTO purchase_orders 
                        (user_client_id, file) 
                        VALUES ($1, $2) returning id`, [idClient, filename]);

        const purchaseOrderId = purchaseOrder.rows[0].id

        await helper.sendEmailPurchaseOrderClient(email, purchaseOrderId)
        await helper.sendEmailPurchaseOrderAdmin(email, purchaseOrderId) 

        return res.status(200).json({ 
            message: "Purchase order created successfully",
        })

    } catch (error) {
        return res.status(500).json( {message: "Internal server error"} )
    }
}

const createPurchaseOrderClient = async (name, email) => {
    try {

        const existingUser = await db.query(`SELECT id, email FROM users WHERE email = $1`, [email]);

        if(existingUser.rowCount !== 0)
            return existingUser.rows[0].id

        const insertedUser = await db.query(`INSERT INTO users 
                        (name, email, role_id) 
                        VALUES ($1, $2, $3) RETURNING id`, [name, email, 4]);return insertedUser.rows[0].id

    } catch (error) {
        return res.status(500).json( {message: "Internal server error"} )
    }
}

const updatePurchaseOrderStatus = async (req, res) => {

    const { id, method } = req.params
    const {value = 0, user = 0, email = ''} = req.body

    const percent = await helper.getCurrentComissionPercentage()
    

    const comission = value * (percent.value / 100);

    let methodString = ''

    switch (method) {

        case 'active':
            methodString = 'Activa'
            break;
        case 'invoice':
            methodString = 'invoice'
            break;
        case 'cancelled-purchase-order':
            methodString = 'cancelled-purchase-order'
            break;
        default:
            break;

    }

    try {

        if(methodString === 'invoice'){
            await db.query(`INSERT INTO invoices_users (value, comission, percent_comission, user_id, purchase_order_id) VALUES ($1, $2, $3, $4, $5)`, [value, comission, percent.value, user, id])
            await helper.sendEmailComissionClient(email, value, comission, percent.value, id)
        }
        
        await db.query(`UPDATE purchase_orders SET status = '${methodString}', value = $1 WHERE id = $2`, [value, id])

        return res.status(200).json({message: "Purchase order status updated successfully"})

    } catch (error) {
        return res.status(500).json( {message: "Internal server error"} )

    }

}

module.exports = {
    getPurchaseOrders: getPurchaseOrders,
    createPurchaseOrders: createPurchaseOrders,
    updatePurchaseOrderStatus: updatePurchaseOrderStatus,
    createPurchaseOrdersClients: createPurchaseOrdersClients,
    getPurchaseOrdersByClient: getPurchaseOrdersByClient
}