const db = require('./db');
const mailer = require('./mailer');

const getCurrentComissionPercentage = async () => {
    try {

        const comission = await db.query(`SELECT id, percentage_comission as value FROM settings WHERE id = 1`)

        return comission.rows[0]

    } catch (error) {return res.status(500).json( {message: "Internal server error"} )
    }
}

const getPurchaseOrderClientByEmail = async (email) => {
    try {

        const existingUser = await db.query(`SELECT id, email FROM users WHERE email = $1`, [email]);

        return existingUser.rows[0].id

    } catch (error) {return res.status(500).json( {message: "Internal server error"} )
    }
}

const sendEmailPurchaseOrderAdmin = async (email, purchaseOrderId) => {
    try {

        const adminUsers = await db.query(`SELECT id, email FROM users WHERE role_id = 1`);

        adminUsers.rows.forEach(admin => {

            let subject = 'Nueva Orden de Compra generada desde Tkotizo'
            let text = `Hola, el cliente ${email} ha generado la siguiente orden de compra: <a href="http://localhost/b2bfronthtml/cms/b2b_business_app_cms/purchase-orders.php">#${purchaseOrderId}</a>`

            mailer.emailTransporter(admin.email, subject, text)

        });

    } catch (error) {return res.status(500).json({ message: 'Internal server error' });
    }
}

const sendEmailPurchaseOrderClient = async (email, purchaseOrderId) => {
    try {

        let subject = 'Nueva Orden de Compra generada desde Tkotizo'
        let text = `Hola, muchas gracias por cotizar con nosotros. Se ha generado la orden de compra #${purchaseOrderId}. Nuestro equipo se estara poniendo en contacto con ustedes para coordinar el siguiente paso.`

        mailer.emailTransporter(email, subject, text)

    } catch (error) {return res.status(500).json({ message: 'Internal server error' });
    }
}

const sendEmailComissionClient = async (email, value, comission, percent, id) => {
    try {

        let subject = 'Nueva Comision generada desde Tkotizo'
        let text = `Hola, has generado una comision de $${parseFloat(comission).toFixed(2)} sobre un total de $${parseFloat(value).toFixed(2)} por un porcentaje de ${percent}%. El Identificador de la orden de compra es #${id}. Gracias por confiar en nosotros.`

        mailer.emailTransporter(email, subject, text)

    } catch (error) {return res.status(500).json({ message: 'Internal server error' });
    }
}

module.exports = {
    getCurrentComissionPercentage,
    getPurchaseOrderClientByEmail,
    sendEmailPurchaseOrderAdmin,
    sendEmailPurchaseOrderClient,
    sendEmailComissionClient
}
