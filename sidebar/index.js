const db = require('../db');

const listSidebar = async (userId, req, res) => {
    try {

        const sidebar = await db.query(
            `SELECT m.* FROM accesses a inner join menu m on a.menu_id = m.id where user_id = $1`, [userId]
        )

        return res.status(200).json({
            message: "Sidebar retreived successfully",
            data: sidebar.rows
        })

    } catch (error) {
        return res.status(500).json( {message: "Internal server error"} )
    }
}

module.exports = {
    listSidebar: listSidebar
}