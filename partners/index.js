const db = require('../db');


const createPartner = async (req, res) => {

    const { name, email } = req.body
    const filename = req.file.filename;
    
    if(!name || !email || !filename)
        return res.status(400).json({ message: "Missing required fields" })


    try {
        
        await db.query("INSERT INTO partners (name, email, img) VALUES ($1, $2, $3)", [name, email, filename])

        return res.status(200).json({ 
            message: "Partner created successfully",
            name: name
        })

    } catch (error) {

        return res.status(500).json({ message: "Internal server error" })
    
    }

}

const getPartnersCms = async (req, res) => {
    
    const itemsPerPage = 30;
    const page = req.query.page || 1

    try {

        const partners = await db.query(
            `SELECT p.id, p.name, email, p.status, p.created_at, p.img, ac.name as category_name
            FROM partners p 
            LEFT JOIN partners_categories pc
            ON p.id = pc.partner_id 
            LEFT JOIN app_categories ac 
            ON pc.app_category = ac.id
            WHERE p.status = 'active'
            ORDER BY id DESC
            LIMIT ${itemsPerPage}
            OFFSET (${page} - 1) * ${itemsPerPage}`
        )

        return res.status(200).json({
            message: "Partners retreived successfully",
            data: partners.rows
        })

    } catch (error) {
        return res.status(500).json( {message: "Internal server error"} )
    }

}

const getPartnersPublic = async (req, res) => {
    
    const itemsPerPage = 20;
    const page = req.query.page || 1

    try {

        const partners = await db.query(
            `SELECT id, name, email, status, img
            FROM partners
            WHERE status = 'active'
            LIMIT ${itemsPerPage}
            OFFSET (${page} - 1) * ${itemsPerPage}`
        )

        return res.status(200).json({
            message: "Partners retreived successfully",
            data: partners.rows
        })

    } catch (error) {
        return res.status(500).json( {message: "Internal server error"} )
    }

}

const updatePartnerStatus = async (req, res) => {

    const { id, method } = req.params

    let methodString = ''

    switch (method) {
        case 'active':
            methodString = 'active'
            break;
        case 'delete':
            methodString = 'deleted'
            break;
        case 'desactivate':
            methodString = 'desactivated'
            break;
        default:
            break;
    }


    try {

        await db.query(`UPDATE partners SET status = '${methodString}' WHERE id = $1`, [id])

        return res.status(200).json({message: "Partner status updated successfully"})

    } catch (error) {

        return res.status(500).json( {message: "Internal server error"} )

    }

}


module.exports = {
    createPartner: createPartner,
    getPartnersCms: getPartnersCms,
    getPartnersPublic: getPartnersPublic,
    updatePartnerStatus: updatePartnerStatus
}