const db = require('../db');
const mailer = require('../mailer');


const getInvoices = async (req, res) => {
    const itemsPerPage = 20;
    const page = req.query.page || 1

    try {

        const invoices = await db.query(
            `SELECT p.id, file, u.email, p.status, p.created_at, p.value, iu.comission
            FROM purchase_orders p
            INNER JOIN users u
            ON p.user_client_id = u.id
            LEFT JOIN invoices_users iu
            ON p.id = iu.purchase_order_id
            WHERE p.status in ('invoice', 'paid', 'cancelled-invoice')
            ORDER BY p.id DESC
            LIMIT ${itemsPerPage}
            OFFSET (${page} - 1) * ${itemsPerPage}
            `
        )

        return res.status(200).json({
            message: "Invoices retreived successfully",
            data: invoices.rows
        })

    } catch (error) {return res.status(500).json( {message: "Internal server error"} )
    }
}

const updateInvoiceStatus = async (req, res) => {

    const { id, email, value = null } = req.body
    let method = req.body.method

    try {


        await db.query(
            `UPDATE purchase_orders SET status = $1 WHERE id = $2`, [method, id]
        )

        if(method === 'paid') {
            if(mailer.emailTransporter('ramonbriceno12@gmail.com', 'Factura pagada', `Se ha pagado la factura #${id} por un valor de $${value.toFixed(2)}`))
                return res.status(200).json({ message: 'Email sent successfully' });
            else
                return res.status(500).json({ message: 'Internal server error' });
        }

    } catch (error) {
return res.status(500).json( {message: "Internal server error"} )

    }
}

module.exports = {
    getInvoices: getInvoices,
    updateInvoiceStatus: updateInvoiceStatus
}