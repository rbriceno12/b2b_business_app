const db = require('../db');


const getCategories = async (req, res) => {
    const itemsPerPage = 30;
    const page = req.query.page || 1

    try {
        const categories = await db.query(
            `SELECT id, name, status, created_at, img
            FROM app_categories
            WHERE status = 'activate'
            ORDER BY id DESC
            LIMIT ${itemsPerPage}
            OFFSET (${page} - 1) * ${itemsPerPage}`
        )
        return res.status(200).json({
            message: "Categories retreived successfully",
            data: categories.rows
        })

    } catch (error) {
        return res.status(500).json( {message: "Internal server error"} )
    }
}

const getCategoriesCms = async (req, res) => {
    const itemsPerPage = 30;
    const page = req.query.page || 1

    try {
        const categories = await db.query(
            `SELECT id, name, status, created_at, img
            FROM app_categories
            ORDER BY id DESC
            LIMIT ${itemsPerPage}
            OFFSET (${page} - 1) * ${itemsPerPage}`
        )

        return res.status(200).json({
            message: "Categories retreived successfully",
            data: categories.rows
        })

    } catch (error) {
        return res.status(500).json( {message: "Internal server error"} )
    }
}

const createCategories = async (req, res) => {
    const {name} = req.body
    const filename = req.file.filename;

    try {
        await db.query(
            `INSERT INTO app_categories (name, img) VALUES ($1, $2)`, [name, filename]
        )

        return res.status(200).json({
            message: "Category created successfully",
        })

    } catch (error) {return res.status(500).json( {message: "Internal server error"} )
    }
}

const updateCategoryStatus = async (req, res) => {
    const {method} = req.params
    const {id} = req.body

    try {
        await db.query(
            `UPDATE app_categories SET status = $1 WHERE id = $2`, [method, id]
        )

        return res.status(200).json({
            message: "Category status updated successfully",
        })

    } catch (error) {return res.status(500).json( {message: "Internal server error"} )
    }

}


module.exports = {
    getCategories: getCategories,
    createCategories: createCategories,
    updateCategoryStatus: updateCategoryStatus,
    getCategoriesCms: getCategoriesCms
}