const db = require('../db');


const getProducts = async (req, res) => {

    const itemsPerPage = 30;
    const page = req.query.page || 1

    try {
        
        const products = await db.query(
            `SELECT p.*, pp.name as partner 
            FROM products p
            LEFT JOIN partners pp
            ON p.partner_id = pp.id
            ORDER BY p.id DESC
            LIMIT ${itemsPerPage}
            OFFSET (${page} - 1) * ${itemsPerPage}`
        )

        return res.status(200).json({
            message: "Products retreived successfully",
            data: products.rows
        })

    } catch (error) {return res.status(500).json({ message: "Internal server error" })
    
    }

}

const getProductsPublic = async (req, res) => {

    const itemsPerPage = 30;
    const page = req.query.page || 1

    try {
        
        const products = await db.query(
            `SELECT p.*, pp.name as partner 
            FROM products p
            LEFT JOIN partners pp
            ON p.partner_id = pp.id
            WHERE p.status <> 'desactivate'
            ORDER BY p.id DESC
            LIMIT ${itemsPerPage}
            OFFSET (${page} - 1) * ${itemsPerPage}`
        )

        return res.status(200).json({
            message: "Products retreived successfully",
            data: products.rows
        })

    } catch (error) {return res.status(500).json({ message: "Internal server error" })
    
    }

}


const createProduct = async (req, res) => {

    

    try {

        const {name, desc} = req.body
        let partnerId = req.body.partner_id
        const filename = req.file.filename; // Get uploaded file details
        
        partnerId == 0 ? partnerId = null : partnerId

        await db.query(
            `INSERT INTO products (name, description, img, partner_id) VALUES ($1, $2, $3, $4)`, [name, desc, filename, partnerId]
        )

        return res.status(200).json({
            message: "Product created successfully",
        })


    } catch (error) {
        return res.status(500).json({ message: "Internal server error" })
    }
  
}

const updateProductStatus = async (req, res) => {
    
    const {id, method} = req.params

    try {

        await db.query(
            `UPDATE products SET status = $1 WHERE id = $2`, [method, id]
        )

        return res.status(200).json({
            message: "Product status updated successfully",
        })

    } catch (error) {
        return res.status(500).json({ message: "Internal server error" })
    }

}

module.exports = {
    getProducts: getProducts,
    createProduct: createProduct,
    getProductsPublic: getProductsPublic,
    updateProductStatus: updateProductStatus
}