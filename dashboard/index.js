const db = require('../db');

const getDashboardData = async (req, res) => {


    try {

        const countClients = await db.query(
            `SELECT COUNT(id) as total
            FROM users
            WHERE status != 'deleted' AND role_id = 4`
        )

        const countInvoices = await db.query(
            `SELECT COUNT(id) as total
            FROM purchase_orders
            WHERE status = 'invoice'`
        )

        const totalInvoices = await db.query(
            `SELECT SUM(value) as total
            FROM purchase_orders
            WHERE status = 'invoice'`
        )

        const countPurchaseOrders = await db.query(
            `SELECT COUNT(id) as total
            FROM purchase_orders`
        )


        const last10Invoices = await db.query(
            `SELECT p.id, file, u.email, p.status, p.created_at, p.value, iu.comission
            FROM purchase_orders p INNER JOIN users u
            ON p.user_client_id = u.id
            LEFT JOIN invoices_users iu
            ON p.id = iu.purchase_order_id
            WHERE p.status = 'invoice'
            ORDER BY p.id DESC LIMIT 10`
        )

        const last10PurchaseOrders = await db.query(
            `SELECT p.id, file, u.email, p.status, p.created_at, value
            FROM purchase_orders p INNER JOIN users u
            ON p.user_client_id = u.id
            WHERE p.status = 'purchase-order'
            ORDER BY p.id DESC LIMIT 10`
        )

        const comissions = await db.query(
            `SELECT SUM(comission) as total FROM invoices_users WHERE status = 'activated'`
        )

        return res.status(200).json({
            message: "Dashboard data retreived successfully",
            data: {
                totalUsers: countClients.rows[0].total,
                countInvoices: countInvoices.rows[0].total,
                totalInvoices: totalInvoices.rows[0].total,
                countPurchaseOrders: countPurchaseOrders.rows[0].total,
                last10Invoices: last10Invoices.rows,
                last10PurchaseOrders: last10PurchaseOrders.rows,
                comissions: comissions.rows[0].total
            }
        })

    } catch (error) {
        return res.status(500).json( {message: "Internal server error"} )
    }
}

module.exports = {
    getDashboardData: getDashboardData
}