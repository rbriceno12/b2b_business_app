const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const db = require('../db');
require('dotenv').config(); // Load environment variables from .env file
const secret = process.env.SECRET_KEY;
const mailer = require('../mailer');

// Function to generate a JWT token
const generateToken = (userId) => {
    const payload = {
      userId,
    };
  
    return jwt.sign(payload, secret, { expiresIn: '24h' }); // Set appropriate expiration time
};
  
  
// Middleware to verify JWT token (replace with protected routes)
const verifyToken = async (req, res, next) => {

    const authHeader = req.headers.authorization;
    if (!authHeader || !authHeader.startsWith('Bearer '))
        return res.status(401).json({ message: 'Unauthorized access.' });

    const token = authHeader.split(' ')[1];

    try {
        
        const decoded = jwt.verify(token, secret);
        req.userId = decoded.userId; // Attach user ID to the request object
        next();

    } catch (error) {

        return res.status(403).json({ message: 'Invalid or expired token.' });

    }
};

const verifyLoggedUser = async (req, res) => {

    const authHeader = req.headers.authorization;
    if (!authHeader || !authHeader.startsWith('Bearer '))
        return res.status(401).json({ message: 'Unauthorized access.' });

    const token = authHeader.split(' ')[1];

    try {
        
        const decoded = jwt.verify(token, secret);
        req.userId = decoded.userId; // Attach user ID to the request object
        return res.status(200).json({ message: 'User logged successfully' })

    } catch (error) {

        return res.status(403).json({ message: 'Invalid or expired token.' });

    }
};


const loginUser = async (req, res) => {

    const { email, password } = req.body;

    if (!email || !password)
        return res.status(400).json({ message: 'Email and password are required.' });

    try {
        const user = await db.query(`SELECT id, password_hash, status, role_id FROM users WHERE email = $1 AND status != 'desactivated' AND status != 'deleted'`, [email]);

        if (user.rowCount === 0)
            return res.status(401).json({ message: 'Invalid email or password.' });

        const passwordMatch = await bcrypt.compare(password, user.rows[0].password_hash);

        if (!passwordMatch)
            return res.status(401).json({ message: 'Invalid email or password.' });

        if (user.rows[0].status != 'active')
            return res.status(401).json({ message: 'User deleted.' });

        const token = generateToken(user.rows[0].id); // Generate JWT token on successful login

        return res.status(200).json({ 
            message: "User logged successfully",
            userId: user.rows[0].id,
            email: email,
            token: token,
            role_id: user.rows[0].role_id
        })

    } catch (error) {

        res.status(500).json({ message: 'Internal server error.' });

    }
}

const registerUser = async (req, res) => {

    const { name, email, password } = req.body;

    if (!name || !email || !password)
        return res.status(401).json({ message: 'name, email and password are required.' });


    const passwordHash = await bcrypt.hash(password, 10);

    // Validate for existing name or email (replace with database checks)
    try {
        
        const existingUser = await db.query('SELECT * FROM users WHERE email = $1', [email]);

        if(existingUser.rowCount !== 0)
            return res.status(401).json({ message: 'User already exists!' });

        const insertedUser = await db.query(`INSERT INTO users 
                        (name, email, password_hash) 
                        VALUES ($1, $2, $3) RETURNING id`, [name, email, passwordHash]);
        
        const userId = insertedUser.rows[0].id;

        const token = generateToken(userId)

        return res.status(200).json({ 
            message: "User logged successfully",
            userId: user.rows[0].id,
            email: email,
            token: token,
            role_id: user.rows[0].role_id
        })
        
    } catch (err) {
        
        res.status(500).json({ message: "Internal server error" });

    }

}

const registerUserNoAuth = async (req, res) => {

    const { name, email, password, role } = req.body;

    if (!name || !email || !password)
        return res.status(400).json({ message: 'name, email and password are required.' });
    console.log(role)

    const passwordHash = await bcrypt.hash(password, 10);

    // Validate for existing name or email (replace with database checks)
    try {
        
        const existingUser = await db.query('SELECT * FROM users WHERE email = $1', [email]);

        if(existingUser.rowCount !== 0)
            return res.status(401).json({ message: 'User already exists!' });

        await db.query(`INSERT INTO users 
                        (name, email, password_hash, role_id) 
                        VALUES ($1, $2, $3, $4) RETURNING id`, [name, email, passwordHash, role]);return res.status(200).json({ 
            message: "User registered successfully",
        })
        
    } catch (err) {
        console.log(err)
        res.status(500).json({ message: "Internal server error" });

    }

}

const sendEmailForgotPassword = async (req, res) => {

    let { email } = req.body;

    try {

        const user = await db.query(`SELECT email FROM users WHERE email = $1`, [email]);

        if (user.rowCount === 0)
            return res.status(401).json({ message: 'Invalid email.' });

        email = user.rows[0].email;
        let subject = 'Forgot password'
        let text = 'Hello, this is a test email for forgot password'

        // Call the emailTransporter function from the mailer.js file
        if(mailer.emailTransporter(email, subject, text))
            return res.status(200).json({ message: 'Email sent successfully' });
        else
            return res.status(500).json({ message: 'Internal server error' });

    }catch (error) {
        ;
        return res.status(500).json({ message: 'Internal server error' });
    };

}

const changeUserPassword = async (req, res) => {

    const { email, password } = req.body;

    if (!email || !password)
        return res.status(400).json({ message: 'email and password are required.' });

    const passwordHash = await bcrypt.hash(password, 10);

    // Validate for existing name or email (replace with database checks)
    try {
        
        const existingUser = await db.query('SELECT * FROM users WHERE email = $1', [email]);

        if(existingUser.rowCount == 0)
            return res.status(401).json({ message: 'Invalid email!' });

        await db.query(`UPDATE users SET password_hash = $1`, [passwordHash]);return res.status(200).json({ 
            message: "Password updated successfully",
        })
        
    } catch (err) {
        
        res.status(500).json({ message: "Internal server error" });

    }
}


module.exports = {
    loginUser: loginUser,
    registerUser: registerUser,
    verifyToken: verifyToken,
    generateToken: generateToken,
    registerUserNoAuth: registerUserNoAuth,
    verifyLoggedUser: verifyLoggedUser,
    sendEmailForgotPassword: sendEmailForgotPassword,
    changeUserPassword: changeUserPassword
}