const nodemailer = require('nodemailer');
require('dotenv').config(); // Load environment variables from .env file

const emailTransporter = async (email, subject, text) => {

    let transporter = nodemailer.createTransport({
        service: 'gmail', // replace with your email provider
        auth: {
            user: process.env.EMAIL_USERNAME,
            pass: process.env.EMAIL_PASSWORD
        },
        tls: {
            rejectUnauthorized: false
        }
    });

    let mailOptions = {
        from: process.env.EMAIL_USERNAME,
        to: email,
        subject: subject,
        html: text
    };

    try {

        await transporter.sendMail(mailOptions);
        return true;

    } catch (error) {
return false;
        
    }

    

}

module.exports = {
    emailTransporter: emailTransporter
}